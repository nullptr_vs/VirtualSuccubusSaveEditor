import json
import hashlib
import os
import re
import sys
import shutil
from datetime import datetime
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from PySide6.QtWidgets import QApplication, QMainWindow, QMessageBox
from mainWindow import Ui_MainWindow

LEWD_OUTFITS = [
    {"id": "Outfit41", "name": "See Through Pink Undies", "Dress Layer": "None", "Skin Layer": "SkinLayers_SeeThroughPinkUndies"},
    {"id": "Outfit99", "name": "Catsuit 1", "Dress Layer": "None", "Skin Layer": "Catsuit"},
    {"id": "Outfit37", "name": "Pink Strings", "Dress Layer": "None", "Skin Layer": "SkinLayers_PinkStrings"},
    {"id": "Outfit24", "name": "BDSM Leather", "Dress Layer": "None", "Skin Layer": "SkinLayers_BDSMLeather"},
    {"id": "Outfit23", "name": "Transparent Linggerie", "Dress Layer": "DressLayers_TransparentLingerie", "Skin Layer": "None"},
    {"id": "Outfit101", "name": "Catsuit 2", "Dress Layer": "None", "Skin Layer": "Catsuit"},
    {"id": "Outfit36", "name": "Pink Ribbon", "Dress Layer": "None", "Skin Layer": "SkinLayers_PinkRibbon"},
    {"id": "Outfit21", "name": "Maid", "Dress Layer": "DressLayers_Maid", "Skin Layer": "SkinLayers_BlackThighHighs"},
    {"id": "Outfit59", "name": "Booty Bandies", "Dress Layer": "HoodieLayers_PurpleCrop", "Skin Layer": "SkinLayers_BootyBandies"},
    {"id": "Outfit46", "name": "Transparent Black", "Dress Layer": "None", "Skin Layer": "SkinLayers_TransparentBlack"},
    {"id": "Outfit48", "name": "Cow", "Dress Layer": "None", "Skin Layer": "SkinLayers_Cow"},
    {"id": "Outfit20", "name": "Bunny", "Dress Layer": "None", "Skin Layer": "SkinLayers_Bunny"},
    {"id": "Outfit38", "name": "Shibari", "Dress Layer": "None", "Skin Layer": "SkinLayers_Shibari"},
    {"id": "Outfit50", "name": "Succu Outfit", "Dress Layer": "DressLayers_SuccuDress", "Skin Layer": "SkinLayers_SuccuUndies"},
    {"id": "Outfit22", "name": "Black Tape", "Dress Layer": "None", "Skin Layer": "SkinLayers_BlackTape"},
    {"id": "Outfit44", "name": "Goth Lingerie", "Dress Layer": "None", "Skin Layer": "SkinLayers_GothLingerie"},
    {"id": "Outfit67", "name": "Laced Suit", "Dress Layer": "None", "Skin Layer": "SkinLayers_LacedSuit"},
    {"id": "Outfit26", "name": "Mesh Leotard", "Dress Layer": "None", "Skin Layer": "SkinLayers_MeshLeotard"},
    {"id": "Outfit1", "name": "Naked?", "Dress Layer": "None", "Skin Layer": "None"},
    {"id": "Outfit25", "name": "Bandaids", "Dress Layer": "None", "Skin Layer": "SkinLayers_Bandaids"},
    {"id": "Outfit58", "name": "Bandages", "Dress Layer": "None", "Skin Layer": "SkinLayers_Bandages"},
    {"id": "Outfit49", "name": "Neko Undies", "Dress Layer": "None", "Skin Layer": "SkinLayers_NekoUndies"}
]

SAVE_DIRECTORY = os.path.join(os.environ.get("userprofile"), "AppData", "LocalLow", "SuccuDev", "Virtual Succubus")
SAVE_FILE_PATH = os.path.join(SAVE_DIRECTORY, "SaveFile.es3")
VS_SAVE_PASSWORD = "VSPassword1"

class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        
        self.applyButton.clicked.connect(self.ApplyChanges)
        self.overrideBoobsSizecheckBox.stateChanged.connect(self.OnBoobsOverrideCheckBoxStateChanged)
        self.boobsSlider.valueChanged.connect(lambda: self.boobsLabel.setText(f"Boobs Size: {self.boobsSlider.value()}"))
        self.openSaveDirectoryPushButton.clicked.connect(lambda: os.startfile(SAVE_DIRECTORY))
        
        self.SaveFileExists()

    def SaveFileExists(self) -> bool:
        if not os.path.exists(SAVE_FILE_PATH):
            QMessageBox.warning(self, "Warning", f"Couldn't find a Virtual Succubus save file at the default location: {SAVE_FILE_PATH}")
            return False
        return True

    def OnBoobsOverrideCheckBoxStateChanged(self):
        if self.overrideBoobsSizecheckBox.isChecked():
            self.boobsSizeNoteLabel.setEnabled(True)
            self.boobsLabel.setEnabled(True)
            self.boobsSlider.setEnabled(True)
        else:
            self.boobsSizeNoteLabel.setEnabled(False)
            self.boobsLabel.setEnabled(False)
            self.boobsSlider.setEnabled(False)
        
    def Decrypt(self) -> dict:
        with open(SAVE_FILE_PATH, "rb") as saveData:
            saveData = saveData.read()

        iv = saveData[:16]
        key = hashlib.pbkdf2_hmac('sha1', VS_SAVE_PASSWORD.encode(), iv, 100, 16)
        cipher = AES.new(key, AES.MODE_CBC, iv)
        decryptedSaveData = cipher.decrypt(saveData[16:])
        
        paddingBytes = decryptedSaveData[-1]
        decryptedSaveData = decryptedSaveData[:-paddingBytes]
            
        return json.loads(decryptedSaveData.decode())
        
    def Encrypt(self, decryptedSaveData: dict) -> bytes:
        saveDataBytes = json.dumps(decryptedSaveData).encode()
        
        iv = get_random_bytes(16)
        key = hashlib.pbkdf2_hmac('sha1', VS_SAVE_PASSWORD.encode(), iv, 100, 16)

        padding_length = 16 - (len(saveDataBytes) % 16)
        if padding_length == 0:
            padding_length = 16

        padding = bytes(chr(padding_length).encode("utf-8") * padding_length)
        saveDataBytes = saveDataBytes + padding

        cipher = AES.new(key, AES.MODE_CBC, iv)
        encryptedData = iv + cipher.encrypt(saveDataBytes)

        return encryptedData

    def UpdateOutfitsToLewdOnly(self, outfitsData: dict):
        outfits = outfitsData["value"].split("|")
        
        for outfit in outfits:
            if "," not in outfit:
                continue
            
            outfit_id, odds = outfit.split(",")
            
            is_lewd = False
            for lewd_outfit in LEWD_OUTFITS:
                if lewd_outfit["id"] in outfit_id:
                    is_lewd = True
                    break
                    
            if is_lewd:
                outfitsData["value"] = re.sub(f"{outfit_id},{odds}", f"{outfit_id},100 ", outfitsData["value"])
            else:
                outfitsData["value"] = re.sub(f"{outfit_id},{odds}", f"{outfit_id},0 ", outfitsData["value"])
                
    def SetValue(self, key: dict, value: float | int | bool | str):
        type = key["__type"]
        if type == "System.Single":
            key["value"] = float(value)
        elif type == "System.Int32":
            key["value"] = int(value)
        elif type == "System.Boolean":
            key["value"] = bool(value)
        elif type == "System.String":
            key["value"] = str(value)
        else:
            print(f"type {type} is not supported!")

    def UpdateSaveData(self) -> dict:
        decryptedSaveData = self.Decrypt()
        
        if self.favorSpinBox.value() > 0:
            self.SetValue(decryptedSaveData["Float_SuccuAppreciation"], self.favorSpinBox.value())
            
        if self.onlyLewdCheckBox.isChecked():
            self.UpdateOutfitsToLewdOnly(decryptedSaveData["String_Data_OutfitOdds"])

        if self.overrideBoobsSizecheckBox.isChecked():
            self.SetValue(decryptedSaveData["Float_BoobSize"], self.boobsSlider.value())
            
        if self.chastityResetCheckBox.isChecked():
            self.SetValue(decryptedSaveData["Int_Special_ChastityDays"], 0)
            
        return decryptedSaveData

    def CreateBackup(self):
        now = datetime.now()
        current_time = now.strftime("%d-%m-%Y-%H-%M-%S")
        shutil.copy2(SAVE_FILE_PATH, os.path.join(SAVE_DIRECTORY, f"SaveFile_BACKUP_{current_time}.es3"))

    def ApplyChanges(self):
        if not self.SaveFileExists():
            return
        
        if self.createSaveBackupCheckBox.isChecked():
            self.CreateBackup()
            
        try:
            updatedSaveData = self.UpdateSaveData()
            encryptedSaveData = self.Encrypt(updatedSaveData)
            
            with open(SAVE_FILE_PATH, "wb") as file:
                file.write(encryptedSaveData)
                
            QMessageBox.information(self, "Info", "Save file updated!")
        except:
            QMessageBox.critical(self, "Error", "An error occurred while updating the save file!")
    
if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = MainWindow()
    window.setWindowTitle("Virtual Succubus Save Editor")
    window.show()

    sys.exit(app.exec())