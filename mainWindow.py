# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QHBoxLayout, QLabel,
    QMainWindow, QMenuBar, QPushButton, QSizePolicy,
    QSlider, QSpacerItem, QSpinBox, QVBoxLayout,
    QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(455, 298)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout_2 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.createSaveBackupCheckBox = QCheckBox(self.centralwidget)
        self.createSaveBackupCheckBox.setObjectName(u"createSaveBackupCheckBox")
        self.createSaveBackupCheckBox.setChecked(True)

        self.verticalLayout.addWidget(self.createSaveBackupCheckBox)

        self.onlyLewdCheckBox = QCheckBox(self.centralwidget)
        self.onlyLewdCheckBox.setObjectName(u"onlyLewdCheckBox")

        self.verticalLayout.addWidget(self.onlyLewdCheckBox)

        self.chastityResetCheckBox = QCheckBox(self.centralwidget)
        self.chastityResetCheckBox.setObjectName(u"chastityResetCheckBox")

        self.verticalLayout.addWidget(self.chastityResetCheckBox)

        self.overrideBoobsSizecheckBox = QCheckBox(self.centralwidget)
        self.overrideBoobsSizecheckBox.setObjectName(u"overrideBoobsSizecheckBox")

        self.verticalLayout.addWidget(self.overrideBoobsSizecheckBox)

        self.boobsSizeNoteLabel = QLabel(self.centralwidget)
        self.boobsSizeNoteLabel.setObjectName(u"boobsSizeNoteLabel")
        self.boobsSizeNoteLabel.setEnabled(False)

        self.verticalLayout.addWidget(self.boobsSizeNoteLabel)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.boobsLabel = QLabel(self.centralwidget)
        self.boobsLabel.setObjectName(u"boobsLabel")
        self.boobsLabel.setEnabled(False)

        self.horizontalLayout_6.addWidget(self.boobsLabel)

        self.boobsSlider = QSlider(self.centralwidget)
        self.boobsSlider.setObjectName(u"boobsSlider")
        self.boobsSlider.setEnabled(False)
        self.boobsSlider.setMinimumSize(QSize(300, 0))
        self.boobsSlider.setMaximumSize(QSize(300, 16777215))
        self.boobsSlider.setMinimum(0)
        self.boobsSlider.setMaximum(150)
        self.boobsSlider.setValue(0)
        self.boobsSlider.setOrientation(Qt.Horizontal)

        self.horizontalLayout_6.addWidget(self.boobsSlider)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_3)


        self.verticalLayout.addLayout(self.horizontalLayout_6)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.favorLabel = QLabel(self.centralwidget)
        self.favorLabel.setObjectName(u"favorLabel")

        self.horizontalLayout.addWidget(self.favorLabel)

        self.favorSpinBox = QSpinBox(self.centralwidget)
        self.favorSpinBox.setObjectName(u"favorSpinBox")
        self.favorSpinBox.setMinimumSize(QSize(100, 0))
        self.favorSpinBox.setMaximum(100000000)
        self.favorSpinBox.setSingleStep(1000)

        self.horizontalLayout.addWidget(self.favorSpinBox)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.applyButton = QPushButton(self.centralwidget)
        self.applyButton.setObjectName(u"applyButton")

        self.horizontalLayout_4.addWidget(self.applyButton)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_2)

        self.openSaveDirectoryPushButton = QPushButton(self.centralwidget)
        self.openSaveDirectoryPushButton.setObjectName(u"openSaveDirectoryPushButton")

        self.horizontalLayout_4.addWidget(self.openSaveDirectoryPushButton)


        self.verticalLayout.addLayout(self.horizontalLayout_4)


        self.verticalLayout_2.addLayout(self.verticalLayout)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 455, 22))
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.createSaveBackupCheckBox.setText(QCoreApplication.translate("MainWindow", u"Create Save Backup", None))
        self.onlyLewdCheckBox.setText(QCoreApplication.translate("MainWindow", u"Only Lewd Outfits", None))
        self.chastityResetCheckBox.setText(QCoreApplication.translate("MainWindow", u"Reset Chastity Sentence", None))
        self.overrideBoobsSizecheckBox.setText(QCoreApplication.translate("MainWindow", u"Override Boobs Size", None))
        self.boobsSizeNoteLabel.setText(QCoreApplication.translate("MainWindow", u"Note: The ingame slider only allows you to change the Boobs Size from 20 up to 95", None))
        self.boobsLabel.setText(QCoreApplication.translate("MainWindow", u"Boobs Size: 0", None))
        self.favorLabel.setText(QCoreApplication.translate("MainWindow", u"Favor:", None))
        self.applyButton.setText(QCoreApplication.translate("MainWindow", u"Apply", None))
        self.openSaveDirectoryPushButton.setText(QCoreApplication.translate("MainWindow", u"Open Save Directory", None))
    # retranslateUi

