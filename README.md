# Virtual Succubus Save Editor
This is a simple save editor for Virtual Succubus which has a couple settings that you can change:
- Only allow lewd outfits
- Reset your chastity sentence
- Override boob size (past their limit) and so on. 
- Your current favor

**Suggestions for more settings are welcome!**

![image showing virtual succubus save editor](assets/preview.png)

## Dependencies
- Python >= 3.6
- PySide6
- pycryptodome

## Usage
0. [Download](https://gitlab.com/nullptr_vs/VirtualSuccubusSaveEditor/-/archive/main/VirtualSuccubusSaveEditor-main.zip) and extract the source code somewhere
1. Install [Python version 3.6](https://www.python.org/downloads/) or higher
2. Open a command prompt inside the extracted folder with the `virtualSuccubusSaveEditor.py` file and type: 
```shell
pip install PySide6 pycryptodome
```

3. Run the script with:
```shell
python virtualSuccubusSaveEditor.py
```
